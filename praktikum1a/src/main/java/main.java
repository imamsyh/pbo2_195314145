/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author IMAM
 */
import javax.swing.JOptionPane;

public class main {

    public static void main(String[] args) {
        buku bk1 = new buku("001", "ALPRO", "2015");
        bk1.setisbn("1010067904123");
        bk1.setHal("150");
        buku bk2 = new buku("002", "PBO1", "2016");
        bk2.setisbn("1001456738929");
        bk2.setHal("160");
        buku bk3 = new buku("003", "PBO2", "2016");
        bk3.setisbn("1003453009746");
        bk3.setHal("200");

        majalah mj1 = new majalah("101", "kompas", "2020");
        mj1.setissn("1002345685433");
        mj1.setVolume("1");
        mj1.setSeries("1");
        majalah mj2 = new majalah("102", "otomotif", "2019");
        mj2.setissn("1004012477328");
        mj2.setVolume("2");
        mj2.setSeries("1");
        majalah mj3 = new majalah("103", "Informatika", "2017");
        mj3.setissn("109373040345");
        mj3.setVolume("1");
        mj3.setSeries("3");

        cd cd1 = new cd("201", "How to be Brave", "2005");
        cd1.setisbn("102835933922");
        cd1.setFormat("tutorial");
        cd cd2 = new cd("202", "Education", "2006");
        cd2.setisbn("102938466389");
        cd2.setFormat("multimedia");
        cd cd3 = new cd("203", "music 1990", "2000");
        cd3.setisbn("120938648393");
        cd3.setFormat("audio");
        System.out.println("------------------------------------------------------");
        System.out.println("Data Peminjaman Perpustakaan Universitas Sanata Dharma");
        System.out.println("------------------------------------------------------");
        int p = Integer.parseInt(JOptionPane.showInputDialog("masukan jumlah pengunjung"));
        int jumlah[] = new int[p];
        for (int i = 0; i < jumlah.length; i++) {

            int n;
            n = Integer.parseInt(JOptionPane.showInputDialog("pilih kategori pengunjung"
                    + "\n 1. Dosen"
                    + "\n 2. Mahasiswa"
                    + "\n 3. Masyarakat"));
            if (n == 1) {
                dosen ds = new dosen();
                ds.setNama(JOptionPane.showInputDialog("masukan nama = "));
                ds.setAlamat(JOptionPane.showInputDialog("masukan alamat = "));
                ds.setNdosen(JOptionPane.showInputDialog("masukan nomor induk pegawai = "));
                System.out.println("Nama Peminjam = " + ds.getNama());
                System.out.println("Kategori = Dosen");
                System.out.println("Alamat = " + ds.getAlamat());
                System.out.println("Nomor Induk Pegawai = " + ds.getNdosen());
                System.out.println("------------------------------------");
            } else if (n == 2) {
                mahasiswa mhs = new mahasiswa();
                mhs.setNama(JOptionPane.showInputDialog("masukan nama = "));
                mhs.setAlamat(JOptionPane.showInputDialog("masukan alamat = "));
                mhs.setNmahasiswa(JOptionPane.showInputDialog("masukan nomor mahasiswa = "));
                System.out.println("Nama Peminjam = " + mhs.getNama());
                System.out.println("Kategori = Mahasiswa");
                System.out.println("Alamat = " + mhs.getAlamat());
                System.out.println("Nomor Mahasiswa = " + mhs.getNmahasiswa());
                System.out.println("------------------------------------");
            } else if (n == 3) {
                masyarakat mas = new masyarakat();
                mas.setNama(JOptionPane.showInputDialog("masukan nama = "));
                mas.setAlamat(JOptionPane.showInputDialog("masukan alamat = "));
                mas.setKtp(JOptionPane.showInputDialog("masukan nomor KTP = "));
                System.out.println("Nama Peminjam = " + mas.getNama());
                System.out.println("Kategori = Masyarakat");
                System.out.println("Alamat = " + mas.getAlamat());
                System.out.println("Nomor Induk Pegawai = " + mas.getKtp());
                System.out.println("------------------------------------");
            }
            int b;
            b = Integer.parseInt(JOptionPane.showInputDialog("masukan jumlah barang yang akan di pinjam"));
            int banyak[]=new int[b];
            for (int j = 0; j < banyak.length; j++) {
                
            
            int x;
            x = Integer.parseInt(JOptionPane.showInputDialog("Pilih Benda yang Akan Dipinjam"
                    + "\n 1.Buku"
                    + "\n 2.Majalah"
                    + "\n 3.CD"));
            if (x == 1) {
                int z;
                z = Integer.parseInt(JOptionPane.showInputDialog("Buku yang tersedia"
                        + "\n Kode-Judul-Tahun"
                        + "\n 001-Alpro-2015"
                        + "\n 002-PBO1-2016"
                        + "\n 003-PBO2-2016"
                        + "Pilih Id Buku"));
                System.out.println("Id | Judul | Tahun | Jumlah Halaman | ISBN");
                if (z == 001) {
                    
                    System.out.println(bk1.getId() + "-" + bk1.getJudul() + "-" + bk1.getTahun() + "-" + bk1.getHal() + "-" + bk1.getisbn());
                } else if (z == 002) {
                    System.out.println(bk2.getId() + "-" + bk2.getJudul() + "-" + bk2.getTahun() + "-" + bk2.getHal() + "-" + bk2.getisbn());

                } else if (z == 003) {
                    System.out.println(bk3.getId() + "-" + bk3.getJudul() + "-" + bk3.getTahun() + "-" + bk3.getHal() + "-" + bk3.getisbn());
                }

            } else if (x == 2) {
                int q;
                q = Integer.parseInt(JOptionPane.showInputDialog("Majalah yang Tersedia"
                        + "\n Kode-Judul-tahun"
                        + "\n 101-Kompas-2020"
                        + "\n 102-Otomotif-2019"
                        + "\n 103-Informatika-2017"));
                System.out.println("Id | Judul | Tahun | Volume | Series | ISBN");
                if (q == 101) {
                    System.out.println(mj1.getId() + "-" + mj1.getJudul() + "-" + mj1.getTahun() + "-" + mj1.getVolume() + "-" + mj1.getSeries() + "-" + bk1.getisbn());
                } else if (q == 102) {
                    System.out.println(mj2.getId() + "-" + mj2.getJudul() + "-" + mj2.getTahun() + "-" + mj2.getVolume() + "-" + mj2.getSeries() + "-" + bk2.getisbn());

                } else if (q == 103) {
                    System.out.println(mj3.getId() + "-" + mj3.getJudul() + "-" + mj3.getTahun() + "-" + mj3.getVolume() + "-" + mj3.getSeries() + "-" + bk3.getisbn());
                }
            } else if (x == 3) {
                int e;
                e = Integer.parseInt(JOptionPane.showInputDialog("CD yang tersedia"
                        + "\n Kode-Judul-Tahun"
                        + "\n 201-How to be Brave-2006"
                        + "\n 202-Education-2005"
                        + "\n 203-Music 1990-2000"));
                System.out.println("Id | Judul | Tahun | Format | ISBN");
                if (e == 201) {
                    System.out.println(cd1.getId() + "-" + cd1.getJudul() + "-" + cd1.getTahun() + "-" + cd1.getFormat() + "-" + cd1.getisbn());
                } else if (e == 202) {
                    System.out.println(cd2.getId() + "-" + cd2.getJudul() + "-" + cd2.getTahun() + "-" + cd2.getFormat() + "-" + cd2.getisbn());
                } else if (e == 203) {
                    System.out.println(cd1.getId() + "-" + cd3.getJudul() + "-" + cd3.getTahun() + "-" + cd3.getFormat() + "-" + cd3.getisbn());
                }
            }int hari,denda;
            hari= Integer.parseInt(JOptionPane.showInputDialog("Lama Peminjaman Barang..Hari"));
            if(hari <= 7){
                denda = 0;
            }else {
                denda = ((hari-7)*500);
            }
            System.out.println("denda = Rp."+denda);
            System.out.println("-----------------------------------");
            }
            
            
            
        }

    }
}
