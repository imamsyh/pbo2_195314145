/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package praktikum3B;

import java.awt.Color;
import java.awt.Container;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

/**
 *
 * @author IMAM
 */
public class lat1 extends JFrame {
    private static final int FRAME_WIDTH = 300;
    private static final int FRAME_HEIGHT = 200;
        private static final int FRAME_X_ORIGIN = 300;
            private static final int FRAME_Y_ORIGIN = 100;
                private static final int BUTTON_WIDTH = 80;
                    private static final int BUTTON_HEIGHT = 40;
                    private final JButton cancelButton;
                    private final JButton okButton;
                    private JTextField txtField;
                    
      public static void main(String[] args) {
        lat1 frame = new lat1();
        frame.setVisible(true);
    }
      public lat1(){
          Container contentPane = getContentPane();
          
          setSize (FRAME_WIDTH, FRAME_HEIGHT);
          setResizable(true);
          setTitle("Program Ch14AbsolutePositioning");
          setLocation (FRAME_X_ORIGIN,FRAME_Y_ORIGIN);
          
          contentPane.setBackground(Color.white);
          contentPane.setLayout(null);
          
          //button
          
          okButton = new JButton("OK");
          okButton.setBounds(150,140,BUTTON_WIDTH,BUTTON_HEIGHT);
          contentPane.add(okButton);
          
          cancelButton = new JButton("CANCEL");
          cancelButton.setBounds(200,165,BUTTON_WIDTH,BUTTON_HEIGHT);
          contentPane.add(cancelButton);
          
          setDefaultCloseOperation(EXIT_ON_CLOSE);
      }

   
    
}
