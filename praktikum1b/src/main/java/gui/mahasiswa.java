/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author IMAM
 */
public class mahasiswa extends penduduk{
    
    private String nim;

    public mahasiswa() {

    }

    public mahasiswa(String nim, String nama, String tanggalLahir) {
        this.nama = nama;
        this.nim = nim;
        this.tanggalLahir = tanggalLahir;

    }

    public void setNim(String nim) {
        this.nim = nim;

    }

    public String getNim() {
        return nim;

    }

    @Override
    public double hitungIuran() {
        double nim1 = Double.parseDouble(nim);
        return nim1 / 10000;

    }

    @Override
    public String toString() {
        double iuran = hitungIuran();
        return nim + "  |\t" + nama + "\t|   " + tanggalLahir + "\t| Rp " + iuran + "\t";
    }

}


