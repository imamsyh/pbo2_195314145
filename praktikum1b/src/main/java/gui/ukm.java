/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author IMAM
 */
public class ukm {
    
    private String namaUnit;
    private mahasiswa ketua;
    private mahasiswa sekretaris;
    private penduduk anggota[];

    public ukm(String namaUnit, mahasiswa ketua, mahasiswa sekretaris, penduduk[] anggota) {
        this.namaUnit = namaUnit;
        this.ketua = ketua;
        this.sekretaris = sekretaris;
        this.anggota = anggota;
        
    }    

    public ukm (String namaUnit, mahasiswa ketua, mahasiswa sekretaris) {
        this.namaUnit = namaUnit;
        this.ketua = ketua;
        this.sekretaris = sekretaris;

    }

    public ukm(String namaUnit) {
        this.namaUnit = namaUnit;

    }

    public ukm() {

    }

    public void setNamaUnit(String unit) {
        this.namaUnit = namaUnit;

    }

    public void setKetua(mahasiswa ketua) {
        this.ketua = ketua;

    }
    
    public void setSekretaris(mahasiswa sekretaris) {
        this.sekretaris = sekretaris;
        
    }
    
    public void setAnggota(penduduk[] anggota) {
        this.anggota = anggota;
        
    }
    
    public String getNama() {
        return namaUnit;
        
    }
    
    public mahasiswa getKetua() {
        return ketua;
        
    }
    
    public mahasiswa getSekretaris() {
        return sekretaris;
        
    }
    
    public penduduk[] getAnggota() {
        return anggota;
                
    }


}


