/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author IMAM
 */
public abstract class penduduk {
    
    protected String nama;
    protected String tanggalLahir;
    
    public penduduk(String nama, String tanggalLahir) {
        this.nama = nama;
        this.tanggalLahir = tanggalLahir;
        
    }
    
    public penduduk() {
        
    }
    
    public void setNama(String nama) {
        this.nama = nama;
        
    }
    
    public String getNama() {
        return nama;
        
    }
    
    public void settanggalLahir(String tanggalLahir) {
        this.tanggalLahir = tanggalLahir;
        
    }
    
    public String gettanggalLahir() {
        return tanggalLahir;
        
    }
    
    abstract public double hitungIuran();
    
    
}


