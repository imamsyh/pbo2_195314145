/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author IMAM
 */
public class main {

    public static void main(String[] args) {
        ukm ukm = new ukm("MANTAP");
        mahasiswa ketua = new mahasiswa("19145", "Muhammad Imam Syahputra", "26-Juli-2001");
        mahasiswa sekretaris = new mahasiswa("19123", "Joko", "19-Februari-2000");

        ukm.setKetua(ketua);
        ukm.setSekretaris(sekretaris);

        mahasiswa anggota1 = new mahasiswa("19100", "Budi", "20-Juni-2001");
        mahasiswa anggota2 = new mahasiswa("19122", "Santi", "25-Desember-1999");
        masyarakat anggota3 = new masyarakat("19002", "Didit", "27-Oktober-2000");
        masyarakat anggota4 = new masyarakat("194570", "Bagas", "25-Maret-2002");

        penduduk anggota[] = new penduduk[4];
        anggota[0] = anggota1;
        anggota[1] = anggota2;
        anggota[2] = anggota3;
        anggota[3] = anggota4;

        double totalIuran = 0;
        System.out.println("Data Anggota UKM ");
        System.out.println("----------------------------------------------------------------------------------------------");
        System.out.println("Nama UKM       :" + ukm.getNama());
        System.out.println("Ketua UKM      :" + ketua.getNama());
        System.out.println("NIM            :" + ketua.getNim());
        System.out.println("Tanggal Lahir  :" + ketua.gettanggalLahir());
        System.out.println();
        System.out.println("Sekretaris UKM :" + sekretaris.getNama());
        System.out.println("NIM            :" + sekretaris.getNim());
        System.out.println("Tangggal Lahir :" + sekretaris.gettanggalLahir());
        System.out.println();

        System.out.println("Daftar Anggota    : \n");
        System.out.println("=========================================================================================");
        System.out.printf("%-5s", "No |     NIM     |       Nama        |       Tanggal Lahir       |       Iuran");
        System.out.printf("\t|\n");
        System.out.printf("---|---------------|-------------|--------------|---------------------------------------|\n");

        for (int i = 0; i < anggota.length; i++) {
            System.out.println(i + 1 + "  |\t" + anggota[i].toString());
            totalIuran += anggota[i].hitungIuran();

            System.out.println("-----------------------------------------------------------------------------------------");
            System.out.printf("\t\t\t\t\t\tTotal Iuran    | Rp " + Math.round(totalIuran));
            System.out.println("\t|");
            System.out.println("=========================================================================================");

        }

    }

}
